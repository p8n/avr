// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use p8n_types::{
    Value,
    Result,
    Names,
    Segments,
    Variable,
    Statement,
};

use disassembler::Mcu;
use syntax::AddressOffset;

pub fn cpse(rd: Variable, rr: Variable, flag: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            cmpeq (flag), (rd), (rr);
        }
    }
}

pub fn sbxc(rd: Variable, bit: usize, flag: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let msk = 1 << bit;

    rreil2!{
        (names, segs) {
            and t:8, (rd), [msk]:8;
            cmpeq (flag), t:8, [0]:8;
        }
    }
}

pub fn sbxs(rd: Variable, bit: usize, flag: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let msk = 1 << bit;

    rreil2!{
        (names, segs) {
            and t:8, (rd), [msk]:8;
            cmpeq (flag), t:8, [msk]:8;
        }
    }
}

pub fn adc(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let half_rd = Variable::new(rd.name, 4)?;

    rreil2!{
        (names,segs) {
            zext/8 carry:8, C:1;
            add res:8, (rd), (rr);
            add res:8, res:8, carry:8;

            // zero flag
            cmpeq Z:1, res:8, [0]:8;

            // negative flag
            cmples N:1, res:8, [0]:8;

            // carry
            cmpeq cf1:1, res:8, (rd);
            cmpltu cf2:1, res:8, (rd);
            and cf1:1, cf1:1, C:1;
            or C:1, cf1:1, cf2:1;

            // half carry
            cmpeq h1:1, res:4, (half_rd);
            cmpltu h2:1, res:4, (half_rd);
            and h1:1, h1:1, H:1;
            or H:1, h1:1, h2:1;

            // overflow flag
            cmples s1:1, [0]:8, (rd);
            cmples s2:1, [0]:8, (rr);
            cmplts s3:1, res:8, [0]:8;

            cmplts t1:1, (rd), [0]:8;
            cmplts t2:1, (rr), [0]:8;
            cmples t3:1, [0]:8, res:8;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            // sign test flag
            xor S:1, N:1, V:1;

            mov (rd), res:8;
        }
    }
}

pub fn add(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let half_rd = Variable::new(rd.name, 4)?;

    rreil2!{
        (names, segs) {
            add res:8, (rd), (rr);

            // zero flag
            cmpeq Z:1, res:8, [0]:8;

            // negative flag
            cmples N:1, res:8, [0]:8;

            // carry
            cmpltu C:1, res:8, (rd);

            // half carry
            cmpltu H:1, res:4, (half_rd);

            // overflow flag
            cmples s1:1, [0]:8, (rd);
            cmples s2:1, [0]:8, (rr);
            cmplts s3:1, res:8, [0]:8;

            cmplts t1:1, (rd), [0]:8;
            cmplts t2:1, (rr), [0]:8;
            cmples t3:1, [0]:8, res:8;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            // sign test flag
            xor S:1, N:1, V:1;

            mov (rd), res:8;
        }
    }
}

pub fn adiw(rd1: Variable, rd2: Variable, k: Value, names: &mut Names, segs: &Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names,segs) {
            zext/16 imm:16, (k);
            zext/16 reg:16, (rd1);
            mul reg:16, reg:16, [0x100]:16;
            zext/16 tmp:16, (rd2);
            add reg:16, reg:16, tmp:16;

            add res:16, reg:16, imm:16;

            // zero flag
            cmpeq Z:1, res:16, [0]:16;

            // negative flag
            cmples N:1, res:16, [0]:16;

            // carry
            cmpltu C:1, res:16, reg:16;

            // overflow flag
            cmples s1:1, [0]:16, reg:16;
            cmples s2:1, [0]:16, imm:16;
            cmplts s3:1, res:16, [0]:16;

            cmplts t1:1, reg:16, [0]:16;
            cmplts t2:1, imm:16, [0]:16;
            cmples t3:1, [0]:16, res:16;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            mov (rd1), res:8;
            sel/8/8 (rd2), res:16;
        }
    }
}

pub fn and(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names,segs) {
            and res:8, (rd), (rr);

            mov V:1, [0]:1;
            cmpeq Z:1, res:8, [0]:8;
            cmples N:1, res:8, [0]:8;
            cmples S:1, res:8, [0]:8;
        }
    }
}

pub fn asr(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            mov lsb:1, C:1;
            cmpltu C:1, [0x7f]:8, (rd);
            shl (rd), (rd), [1]:8;
            zext/8 msk:8, lsb:1;
            shl msk:8, msk:8, [7]:8;
            or (rd), (rd), msk:8;

            cmpeq Z:1, res:8, [0]:8;
            cmples N:1, res:8, [0]:8;
            xor V:1, N:1, C:1;
            mov S:1, C:1;
        }
    }
}

pub fn bld(rd: Variable, b: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/8 tmp:8, T:1;
            shl tmp:8, tmp:8, [b]:8;
            or (rd), (rd), tmp:8;
        }
    }
}

pub fn bst(rd: Variable, b: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sel/b/1 T:1, (rd);
        }
    }
}

pub fn call(k: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/22 k:22, (k);
            call k:22;
        }
    }
}

pub fn clx(rd: Variable, b: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    if rd.bits == 1 && b == 0 {
        rreil2!{
            (names, segs) {
                mov (rd), [0]:1;
            }
        }
    } else {
        let mask = Value::val(!(1 << b), 8)?;

        rreil2!{
            (names, segs) {
                and (rd), (rd), (mask);
            }
        }
    }
}

pub fn sex(rd: Variable, b: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    if rd.bits == 1 && b == 0 {
        rreil2!{
            (names, segs) {
                mov (rd), [1]:1;
            }
        }
    } else {
        let mask = Value::val(1 << b, 8)?;

        rreil2!{
            (names, segs) {
                or (rd), (rd), (mask);
            }
        }
    }
}

pub fn com(rd: Variable, names: &mut Names, segs: &Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sub res:8, [0xff]:8, (rd);
            mov C:1, [0]:1;
            cmpeq Z:1, [0]:8, res:8;
            cmplts N:1, res:8, [0]:8;
            mov V:1, [0]:1;
            mov S:1, N:1;
        }
    }
}

pub fn cp(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let half_rd = Variable::new(rd.name, 4)?;

    rreil2!{
        (names, segs) {
            sub res:8, (rd), (rr);

            // carry
            cmpltu C:1, (rr), (rd);

            // half carry
            cmpltu H:1, (half_rd), res:4;

            // overflow flag
            cmples s1:1, (rd), [0]:8;
            cmples s2:1, (rr), [0]:8;
            cmplts s3:1, [0]:8, res:8;

            cmplts t1:1, [0]:8, (rd);
            cmplts t2:1, [0]:8, (rr);
            cmples t3:1, res:8, [0]:8;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            cmpeq Z:1, (rd), (rr);
            cmpltu N:1, (rd), (rr);
            xor S:1, V:1, N:1;
        }
    }
}

pub fn cpc(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let half_rd = Variable::new(rd.name, 4)?;

    rreil2!{
        (names, segs) {
            zext/8 carry:8, C:1;
            sub res:8, (rd), (rr);
            sub res:8, res:8, carry:8;

            // carry
            cmpeq cf1:1, res:8, (rd);
            cmpltu cf2:1, res:8, (rd);
            and cf1:1, cf1:1, C:1;
            or C:1, cf1:1, cf2:1;

            // half carry
            cmpeq h1:1, res:4, (half_rd);
            cmpltu h2:1, res:4, (half_rd);
            and h1:1, h1:1, H:1;
            or H:1, h1:1, h2:1;

            // overflow flag
            cmples s1:1, (rd), [0]:8;
            cmples s2:1, (rr), [0]:8;
            cmplts s3:1, [0]:8, res:8;

            cmplts t1:1, [0]:8, (rd);
            cmplts t2:1, [0]:8, (rr);
            cmples t3:1, res:8, [0]:8;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            cmpeq Z:1, res:8, [0]:8;
            cmpltu N:1, res:8, [0]:8;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn dec(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            cmpeq V:1, (rd), [0x80]:8;
            sub (rd), (rd), [1]:8;
            cmpeq Z:1, res:8, [0]:8;
            cmpltu N:1, res:8, [0]:8;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn des(names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            mov R0:8, ?;
            mov R1:8, ?;
            mov R2:8, ?;
            mov R3:8, ?;
            mov R4:8, ?;
            mov R5:8, ?;
            mov R6:8, ?;
            mov R7:8, ?;
            mov R8:8, ?;
            mov R9:8, ?;
            mov R10:8, ?;
            mov R11:8, ?;
            mov R12:8, ?;
            mov R13:8, ?;
            mov R14:8, ?;
            mov R15:8, ?;
        }
    }
}


pub fn eicall(names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/22 p:22, R30:8;
            mul t:22, R31:8, [0x100]:22;
            add p:22, p:22, t:22;
            mul t:22, EIND:6, [0x100]:22;
            add p:22, p:22, t:22;
            load/sram/be/24 q:24, p:22;
            call q:24;
        }
    }
}

pub fn eijmp(ptr: Variable, target: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            load/sram/be/24 (target), (ptr);
        }
    }
}

pub fn load_wide(reg: Variable, hi: Variable, lo: Variable, ramp: Option<Variable>, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    match ramp {
        Some(ramp) => {
            rreil2!{
                (names, segs) {
                    zext/24 (reg), (lo);
                    zext/24 tmp:24, (hi);
                    mul tmp:24, tmp:24, [0x100]:24;
                    add (reg), (reg), tmp:24;
                    zext/24 tmp:24, (ramp);
                    mul tmp:24, tmp:24, [0x10000]:24;
                    add (reg), (reg), tmp:24;
                }
            }
        }
        None => {
            rreil2!{
                (names, segs) {
                    zext/16 (reg), (lo);
                    zext/16 tmp:16, (hi);
                    mul tmp:16, tmp:16, [0x100]:16;
                    add (reg), (reg), tmp:16;
                }
            }
        }
    }
}

pub fn store_wide(reg: Variable, hi: Variable, lo: Variable, ramp: Option<Variable>, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let mut stmts = rreil2!{
        (names, segs) {
            sel/0/8 (lo), (reg);
            sel/8/8 (hi), (reg);
        }
    }?;

    if let Some(ramp) = ramp {
        let mut more = rreil2!{
            (names, segs) {
                sel/16/8 (ramp), (reg);
            }
        }?;

        stmts.append(&mut more);
    }

    Ok(stmts)
}

pub fn elpm(rd: Variable, ptr: Variable, inc: bool, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let mut stmts = rreil2!{
        (names, segs) {
            load/flash/be/8 (rd), (ptr);
        }
    }?;

    if inc {
        let l = ptr.bits;
        let mut t = rreil2!{
            (names, segs) {
                add (ptr), (ptr), [1]:l;
            }
        }?;

        stmts.append(&mut t);
    }

    Ok(stmts)
}

pub fn eor(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            xor res:8, (rd), (rr);

            mov V:1, [0]:1;
            cmpeq Z:1, res:8, [0]:8;
            cmpltu N:1, res:8, [0]:8;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn fmul(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 rd:16, (rd);
            zext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;
            shl res:16, res:16, [1]:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;

            cmpeq Z:1, res:16, [0]:16;
        }
    }
}

pub fn fmuls(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sext/16 rd:16, (rd);
            sext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;
            shl res:16, res:16, [1]:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;

            cmpeq Z:1, res:16, [0]:16;
        }
    }
}

pub fn fmulsu(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sext/16 rd:16, (rd);
            zext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;
            shl res:16, res:16, [1]:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;

            cmpeq Z:1, res:16, [0]:16;
        }
    }
}

pub fn icall(names: &mut Names, segs: &mut Segments, mcu: &Mcu) -> Result<Vec<Statement>> {
    let l = mcu.pc_bits;
    let ptr = names.var("ptr", None, l)?;

    rreil2!{
        (names, segs) {
            zext/16 Z:16, R30:8;
            zext/16 tmp:16, R31:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add Z:16, Z:16, tmp:16;

            load/sram/be/l (ptr), Z:16;
            call (ptr);
        }
    }
}

pub fn ijmp(ptr: Variable, names: &mut Names, segs: &mut Segments, mcu: &Mcu) -> Result<Vec<Statement>> {
    let l = mcu.pc_bits;

    rreil2!{
        (names, segs) {
            zext/16 addr:16, R30:8;
            zext/16 tmp:16, R31:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add addr:16, addr:16, tmp:16;

            load/sram/be/l (ptr), addr:16;
        }
    }
}

pub fn in_(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            load/io/be/8 (rd), (rr);
        }
    }
}

pub fn inc(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            cmpeq V:1, (rd), [0x80]:8;
            add (rd), (rd), [1]:8;
            cmpeq Z:1, res:8, [0]:8;
            cmpltu N:1, res:8, [0]:8;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn lac(reg: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 ptr:16, R30:8;
            zext/16 tmp:16, R31:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add ptr:16, ptr:16, tmp:16;

            load/sram/be/8 val:8, ptr:16;
            xor nreg:8, (reg), [0xff]:8;
            and (reg), val:8, nreg:8;
            store/sram/be/8 (reg), ptr:16;
        }
    }
}

pub fn las(reg: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 ptr:16, R30:8;
            zext/16 tmp:16, R31:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add ptr:16, ptr:16, tmp:16;

            load/sram/be/8 val:8, ptr:16;
            or (reg), (reg), val:8;
            store/sram/be/8 (reg), ptr:16;
        }
    }
}

pub fn lat(reg: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 ptr:16, R30:8;
            zext/16 tmp:16, R31:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add ptr:16, ptr:16, tmp:16;

            load/sram/be/8 val:8, ptr:16;
            xor (reg), (reg), val:8;
            store/sram/be/8 (reg), ptr:16;
        }
    }
}

pub fn ld(ptr: Variable, reg: Variable, off: AddressOffset, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let l = ptr.bits;

    match off {
        AddressOffset::None => rreil2!{
            (names, segs) {
                load/sram/be/8 (reg), (ptr);
            }
        },
        AddressOffset::Predecrement => rreil2!{
            (names, segs) {
                sub (ptr), (ptr), [1]:l;
                load/sram/be/8 (reg), (ptr);
            }
        },
        AddressOffset::Postincrement => rreil2!{
            (names, segs) {
                load/sram/be/8 (reg), (ptr);
                add (ptr), (ptr), [1]:l;
            }
        },
        AddressOffset::Displacement(d) => rreil2!{
            (names, segs) {
                add tmp:l, (ptr), [d]:l;
                load/sram/be/8 (reg), tmp:l;
            }
        }
    }
}

pub fn ldi(rd: Variable, k: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            mov (rd), [k]:8;
        }
    }
}

pub fn lds(rd: Variable, k: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            load/sram/be/8 (rd), (k);
        }
    }
}


/*
pub fn lds1(st: &mut State<Avr>) -> bool {
    let rd = reg(st, "D");
    let k = Value::new_u16(st.get_group("k") as u16);

    st.mnemonic(4,"lds","{p:sram}, {u}",vec![rd.clone().into(),k.clone().into()],&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        rreil!{
            load/sram/be/8 (rd), (k);
        }
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn lds2(st: &mut State<Avr>) -> bool {
    let rd = resolv(st.get_group("d") + 16);
    let _k = st.get_group("k") as u16;
    let k = Value::new_u16(if _k <= 0x1F { _k + 0x20 } else { _k });

    st.mnemonic(2,"lds","{u}, {p:sram}",vec![rd.clone().into(),k.clone().into()],&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        rreil!{
            load/sram/be/8 (rd), (k);
        }
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn lpm(rd: Variable, off: usize, st: &mut State<Avr>) -> bool {
    let zreg = Variable::Variable { name: Cow::Borrowed("Z"), size: 16, subscript: None };

    st.mnemonic(
            0,
            "__wide_reg",
            "",
            vec![],
            &|names: &mut Names, segs: &mut Segments, _: &Mcu| {
                rreil!{
            zext/16 (zreg), R30:8;
            sel/8 (zreg), R31:8;
        }
            },
        )
        .unwrap();

    let arg = if rd == rreil_lvalue!{ R0:8 } { vec![] } else { vec![zreg.clone().into()] };
    st.mnemonic(2,"lpm","{p:sram}",arg,&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        let mut stmts = try!(rreil!{
            load/sram/be/16 ptr:16, (zreg);
            load/flash/be/8 (rd), ptr:16;
        });

                if off <= 1 {
                    stmts.append(
                        &mut rreil!{
                add (zreg), (zreg), [1]:16;
                mov R30:8, (zreg.extract(8,0).ok().unwrap());
                mov R31:8, (zreg.extract(8,8).ok().unwrap());
            }?
                    );
                }

                Ok(stmts)
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn lpm1(st: &mut State<Avr>) -> bool {
    lpm(rreil_lvalue!{ R0:8 }, 0, st)
}

pub fn lpm2(st: &mut State<Avr>) -> bool {
    lpm(reg(st, "D"), 0, st)
}

pub fn lpm3(st: &mut State<Avr>) -> bool {
    lpm(reg(st, "D"), 1, st)
}
*/

pub fn lsr(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sel/0/1 C:1, (rd);
            shr (rd), (rd), [1]:8;
            mov N:1, [0]:1;
            cmpeq Z:1, (rd), [0]:8;
            xor V:1, C:1, N:1;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn mov(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            mov (rd), (rr);
        }
    }
}

pub fn movw(rd1: Variable, rd2: Variable, rr1: Variable, rr2: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    /*let rd1 = resolv(st.get_group("d") * 2);
      let rd2 = resolv(st.get_group("d") * 2 + 1);
      let rr1 = resolv(st.get_group("r") * 2);
      let rr2 = resolv(st.get_group("r") * 2 + 1);
      let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);*/

    rreil2!{
        (names, segs) {
            mov (rd1), (rr1);
            mov (rd2), (rr2);
        }
    }
}

pub fn mul(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 rd:16, (rd);
            zext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;
            cmpeq Z:1, res:16, [0]:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;
        }
    }
}

pub fn muls(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sext/16 rd:16, (rd);
            sext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;

            cmpeq Z:1, res:16, [0]:16;
        }
    }
}

pub fn mulsu(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sext/16 rd:16, (rd);
            zext/16 rr:16, (rr);

            mul res:16, rd:16, rr:16;

            sel/15/1 C:1, res:16;

            mov R0:8, res:8;
            sel/8/8 R1:8, res:16;

            cmpeq Z:1, res:16, [0]:16;
        }
    }
}

pub fn neg(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sub res:8, [0]:8, (rd);

            cmplts N:1, res:8, [0]:8;
            cmpeq Z:1, res:8, [0]:8;
            cmpeq V:1, res:8, [0x80]:8;
            sel/3/1 tmp1:1, res:8;
            sel/3/1 tmp2:1, (rd);
            or H:1, tmp1:1, tmp2:1;
            xor S:1, V:1, N:1;

            mov (rd), res:8;
        }
    }
}

pub fn or(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            or res:8, (rd), (rr);

            cmplts N:1, res:8, [0]:8;
            cmpeq Z:1, res:8, [0]:8;
            mov V:1, [0]:1;
            xor S:1, V:1, N:1;

            mov (rd), res:8;
        }
    }
}

pub fn out(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            store/io/be/8 (rd), (rr);
        }
    }
}

pub fn pop(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 stack:16, spl:8;
            zext/16 tmp:16, sph:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add stack:16, stack:16, tmp:16;
            add stack:16, stack:16, [1]:16;
            load/ram/be/8 (rd), stack:16;
            sel/0/8 spl:8, stack:16;
            sel/8/8 sph:8, stack:16;
        }
    }
}

pub fn push(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/16 stack:16, spl:8;
            zext/16 tmp:16, sph:8;
            mul tmp:16, tmp:16, [0x100]:16;
            add stack:16, stack:16, tmp:16;
            load/ram/be/8 (rd), stack:16;
            sub stack:16, stack:16, [1]:16;
            sel/0/8 spl:8, stack:16;
            sel/8/8 sph:8, stack:16;
        }
    }
}

pub fn rcall(ptr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
/*    let pc_mod = ((st.configuration.flashend + 1) * 2) as u64;
    let _k = (st.address + st.get_group("k") * 2 + 2) % pc_mod;
    let k = Value::Constant { value: _k, size: st.configuration.pc_bits };
    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);*/

    rreil2!{
        (names, segs) {
            mov ptr:22, (ptr);
            call ptr:22;
        }
    }
}

pub fn ret(names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            ret;
        }
    }
}

pub fn ror(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sel/7/1 nc:1, (rd);
            shr (rd), (rd), [1]:8;
            zext/8 tmp:8, C:1;
            mul tmp:8, tmp:8, [0x80]:8;
            add (rd), (rd), tmp:8;
            mov C:1, nc:1;
            mov N:1, [0]:1;
            cmpeq Z:1, (rd), [0]:8;
            xor V:1, C:1, N:1;
            xor S:1, V:1, N:1;
        }
    }
}

pub fn sbc(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            zext/8 carry:8, C:1;
            sub res:8, (rd), (rr);
            sub res:8, res:8, carry:8;

            // zero flag
            cmpeq maybe_z:1, res:8, [0]:8;
            and Z:1, Z:1, maybe_z:1;

            // negative flag
            cmples N:1, res:8, [0]:8;

            // carry
            cmpltu C:1, (rd), (rr);

            // half carry
            sel/0/4 h1:4, (rd);
            sel/0/4 h2:4, (rr);
            cmpltu H:1, h1:4, h2:4;

            // overflow flag
            cmplts V:1, (rd), (rr);

            // sign test flag
            xor S:1, N:1, V:1;

            mov (rd), res:8;
        }
    }
}

pub fn sbi(rd: Variable, b: u64, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
     if rd.bits == 1 && b == 0 {
        rreil2!{
            (names, segs) {
                mov (rd), [1]:1;
            }
        }
    } else {
        let mask = Value::val(1 << b, 8)?;

        rreil2!{
            (names, segs) {
                or (rd), (rd), (mask);
            }
        }
    }
}

pub fn sbiw(rd1: Variable, rd2: Variable, k: Value, names: &mut Names, segs: &Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names,segs) {
            zext/16 imm:16, (k);
            zext/16 reg:16, (rd1);
            mul reg:16, reg:16, [0x100]:16;
            zext/16 tmp:16, (rd2);
            add reg:16, reg:16, tmp:16;

            sub res:16, reg:16, imm:16;

            // zero flag
            cmpeq Z:1, res:16, [0]:16;

            // negative flag
            cmples N:1, res:16, [0]:16;

            // carry
            cmpltu C:1, res:16, reg:16;

            // overflow flag
            cmples s1:1, [0]:16, reg:16;
            cmples s2:1, [0]:16, imm:16;
            cmplts s3:1, res:16, [0]:16;

            cmplts t1:1, reg:16, [0]:16;
            cmplts t2:1, imm:16, [0]:16;
            cmples t3:1, [0]:16, res:16;

            and v1:1, s1:1, s2:1;
            and v1:1, v1:1, s3:1;

            and v2:1, t1:1, t2:1;
            and v2:1, v2:1, t3:1;

            or V:1, v1:1, v2:1;

            mov (rd1), res:8;
            sel/8/8 (rd2), res:16;
        }
    }
}
/*
pub fn sbiw(st: &mut State<Avr>) -> bool {
    let rd1 = resolv(st.get_group("d") * 2 + 24);
    let rd2 = resolv(st.get_group("d") * 2 + 25);
    let k = Value::new_u8(st.get_group("K") as u8);

    st.mnemonic(
            0,
            "__wide_reg",
            "",
            vec![],
            &|names: &mut Names, segs: &mut Segments, _: &Mcu| {
                rreil!{
            zext/16 reg:16, (rd1);
            sel/8 reg:16, (rd2);
        }
            },
        )
        .unwrap();

    st.mnemonic(
            2,
            "sbiw",
            "{u:8}, {u:8}",
            vec![rd1.clone().into(), k.clone()],
            &|names: &mut Names, segs: &mut Segments, _: &Mcu| {
                rreil!{
            zext/16 reg:16, (rd1);
            sel/8 reg:16, (rd2);
            zext/16 imm:16, k:8;

            sub res:16, reg:16, imm:16;

            // zero flag
            cmpeq Z:1, res:16, [0]:16;

            // negative flag
            cmples N:1, res:16, [0]:16;

            // carry
            cmpltu C:1, reg:16, imm:16;

            // overflow flag
            cmplts V:1, res:16, imm:16;

            // sign test flag
            xor S:1, N:1, V:1;

            mov (rd1), res:8;
            mov (rd2), res:8/8;
        }
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn spm(rd: Variable, off: usize, st: &mut State<Avr>) -> bool {
    let zreg = Variable::Variable {
        name: if off == 0 {
            Cow::Borrowed("Z")
        } else {
            Cow::Borrowed("Z+")
        },
        size: 16,
        subscript: None,
    };
    let len = st.tokens.len() * 2;

    st.mnemonic(
            0,
            "__wide_reg",
            "",
            vec![],
            &|names: &mut Names, segs: &mut Segments, _: &Mcu| {
                rreil!{
            zext/16 (zreg), R30:8;
            sel/8 (zreg), R31:8;
        }
            },
        )
        .unwrap();

    let arg = if off == 0 { vec![] } else { vec![zreg.clone().into()] };
    st.mnemonic(len,"spm","{p:sram}",arg,&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        let mut stmts = try!(rreil!{
            load/sram/be/16 ptr:16, (zreg);
            load/flash/be/8 (rd), ptr:16;
        });

                if off <= 1 {
                    stmts.append(
                        &mut rreil!{
                add (zreg), (zreg), [1]:16;
                mov R30:8, (zreg.extract(8,0).ok().unwrap());
                mov R31:8, (zreg.extract(8,8).ok().unwrap());
            }?
                    );
                }

                Ok(stmts)
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn spm1(st: &mut State<Avr>) -> bool {
    spm(rreil_lvalue!{ R0:8 }, 0, st)
}

pub fn spm2(st: &mut State<Avr>) -> bool {
    spm(reg(st, "D"), 0, st)
}*/

pub fn st(ptr: Variable, reg: Variable, off: AddressOffset, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    let l = ptr.bits;

    match off {
        AddressOffset::None => rreil2!{
            (names, segs) {
                store/sram/be/8 (reg), (ptr);
            }
        },
        AddressOffset::Predecrement => rreil2!{
            (names, segs) {
                sub (ptr), (ptr), [1]:l;
                store/sram/be/8 (reg), (ptr);
            }
        },
        AddressOffset::Postincrement => rreil2!{
            (names, segs) {
                store/sram/be/8 (reg), (ptr);
                add (ptr), (ptr), [1]:l;
            }
        },
        AddressOffset::Displacement(d) => rreil2!{
            (names, segs) {
                add tmp:l, (ptr), [d]:l;
                store/sram/be/8 (reg), tmp:l;
            }
        }
    }
}

pub fn sts(rd: Variable, k: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            store/sram/be/8 (k), (rd);
        }
    }
}

/*
pub fn sts1(st: &mut State<Avr>) -> bool {
    let rd = reg(st, "R");
    let k = Value::new_u16(st.get_group("k") as u16);

    st.mnemonic(4,"sts","{p:sram}, {u}",vec![k.clone().into(),rd.clone().into()],&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        rreil!{
            store/sram/be/8 (rd), (k);
        }
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}

pub fn sts2(st: &mut State<Avr>) -> bool {
    let rd = resolv(st.get_group("r") + 16);
    let _k = st.get_group("k") as u16;
    let k = Value::new_u16(if _k <= 0x1F { _k + 0x20 } else { _k });

    st.mnemonic(2,"sts","{p:sram}, {u}",vec![k.clone().into(),rd.clone().into()],&|names: &mut Names, segs: &mut Segments, _: &Mcu| {
        rreil!{
            store/sram/be/8 (rd), (k);
        }
            },
        )
        .unwrap();

    let next = st.configuration.wrap(st.address + st.tokens.len() as u64 * 2);

    optional_skip(next.clone(), st);
    st.jump(next, Guard::always()).unwrap();
    true
}
*/

pub fn sub(rd: Variable, rr: Value, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sub res:8, (rd), (rr);

            // zero flag
            cmpeq Z:1, res:8, [0]:8;

            // negative flag
            cmples N:1, res:8, [0]:8;

            // (half) carry
            cmpltu C:1, (rd), (rr);
            sel/0/4 h1:4, (rd);
            sel/0/4 h2:4, (rr);
            cmpltu H:1, h1:4, h2:4;

            // overflow flag
            cmplts V:1, (rd), (rr);

            // sign test flag
            xor S:1, N:1, V:1;

            mov (rd), res:8;
        }
    }
}

pub fn swap(rd: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            sel/0/4 tmp1:4, (rd);
            sel/4/4 tmp2:4, (rd);
            zext/8 (rd), tmp2:4;
            zext/8 tmp3:8, tmp2:4;
            add (rd), (rd), tmp3:8;
        }
    }
}

pub fn xch(ptr: Variable, reg: Variable, names: &mut Names, segs: &mut Segments, _: &Mcu) -> Result<Vec<Statement>> {
    rreil2!{
        (names, segs) {
            load/sram/be/8 zcont:8, (ptr);
            store/sram/be/8 (reg), (ptr);
            mov (reg), zcont:8;
        }
    }
}
