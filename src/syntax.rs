// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::ops::Range;

use smallvec::SmallVec;
use p8n_types::{
    Names,
    Segments,
    Result,
    Match,
    Value,
    Statement,
    Variable,
    Guard,
    Region,
    Endianess,
};
use semantic;
use disassembler::*;

#[derive(Clone,Copy,PartialEq,Eq,Debug)]
pub enum AddressOffset {
    Predecrement,
    Postincrement,
    None,
    Displacement(u8),
}

fn collect_bits(mask: u16, opcode: u16) -> u16 {
    let mut ret = 0;
    let mut bit = 0b1000000000000000;

    for _ in 0..16 {
        if bit & mask != 0 {
            ret <<= 1;
            if bit & opcode != 0 {
                ret |= 1;
            }
        }

        bit >>= 1;
    }

    ret
}

pub fn binary(mnemonic: &'static str, a: Value, b: Value, addr: u64, stmts: Vec<Statement>, mcu: &Mcu) -> Result<Match> {
    let m = Match{
        area: addr..(addr + 2),
        opcode: mnemonic.into(),
        operands: SmallVec::from_vec(vec![a,b]),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (addr, Value::val((addr + 2) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::True)
        ]),
    };

    Ok(m)
}

pub fn unary(mnemonic: &'static str, a: Value, addr: u64, stmts: Vec<Statement>, mcu: &Mcu) -> Result<Match> {
    let m = Match{
        area: addr..(addr + 2),
        opcode: mnemonic.into(),
        operands: SmallVec::from_vec(vec![a]),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (addr, Value::val((addr + 2) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::True)
        ]),
    };

    Ok(m)
}

pub fn nonary(mnemonic: &'static str, addr: u64, stmts: Vec<Statement>, mcu: &Mcu) -> Result<Match> {
    let m = Match{
        area: addr..(addr + 2),
        opcode: mnemonic.into(),
        operands: SmallVec::default(),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (addr, Value::val((addr + 2) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::True)
        ]),
    };

    Ok(m)
}

pub fn mnemonic(mnemonic: &'static str, _: &'static str, ops: Vec<Value>, area: Range<u64>, stmts: Vec<Statement>, mcu: &Mcu) -> Result<Match> {
    let m = Match{
        area: area.clone(),
        opcode: mnemonic.into(),
        operands: SmallVec::from_vec(ops),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (area.start, Value::val(area.end % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::True)
        ]),
    };

    Ok(m)
}

pub fn branch(mnemonic: &'static str, addr: u64, f: Variable, expected: bool, target: i64, names: &mut Names, mcu: &Mcu) -> Result<Match> {
    let v = if target >= 0b100_0000 {
        target - 0b1_000_0000
    } else {
        target
    } * 2;
    let v = names.var(format!(".{:+}", v), None, mcu.pc_bits + 1)?;
    let target = Value::val(target as u64, 8)?;
    let m = Match{
        area: addr..(addr + 2),
        opcode: mnemonic.into(),
        operands: SmallVec::from_vec(vec![v.into()]),
        instructions: Vec::default(),
        jumps: SmallVec::from_vec(vec![
          (addr, target, Guard::Predicate{ flag: f, expected: expected }),
          (addr, Value::val((addr + 2) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::Predicate{ flag: f, expected: !expected }),
        ]),
    };

    Ok(m)
}

pub fn shadow(mnemonic: &'static str, addr: u64, stmts: Vec<Statement>, mcu: &Mcu) -> Result<Match> {
    let m = Match{
        area: addr..addr,
        opcode: mnemonic.into(),
        operands: SmallVec::default(),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (addr, Value::val(addr % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::True)
        ]),
    };

    Ok(m)
}

pub fn jump(mnemonic: &'static str, target: Value, real_target: Option<Value>, addr: u64, stmts: Vec<Statement>) -> Result<Match> {
    let m = Match{
        area: addr..(addr + 2),
        opcode: mnemonic.into(),
        operands: SmallVec::from_vec(vec![target]),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (addr, real_target.unwrap_or(target), Guard::True)
        ]),
    };

    Ok(m)
}

pub fn skip(mnemonic: &'static str, address: u64, rd: Value, rr: Value, flag: Variable, stmts: Vec<Statement>, mcu: &Mcu) -> Result<(Guard,Match)> {
    let g = Guard::Predicate{ flag: flag, expected: true };
    let m = Match{
        opcode: mnemonic.into(),
        area: address..(address + 2),
        operands: SmallVec::from_vec(vec![rd.into(),rr.into()]),
        instructions: stmts,
        jumps: SmallVec::from_vec(vec![
            (address, Value::val((address + 2) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, Guard::Predicate{ flag: flag, expected: false }),
        ]),
    };

    Ok((g, m))
}

pub fn decode_register(mask: u16, opcode: u16, names: &mut Names) -> Result<Variable> {
    match collect_bits(mask, opcode) {
        r@0...31 => names.var(format!("R{}", r), None, 8),
        r => Err(format!("no such register: {}", r).into()),
    }
}

pub fn decode_io_register(mask: u16, opcode: u16, names: &mut Names) -> Result<Variable> {
    names.var(format!("0x{:0>2X}", collect_bits(mask, opcode)), None, 8)
}

fn complete_skip_match(skip: &mut Option<(Guard, usize)>, this: &Match, mcu: &Mcu, matches: &mut Vec<Match>) -> Result<()> {
    match skip {
        &mut Some((ref guard, p)) => {
            let s = matches[p].area.start;
            matches[p].jumps.push((s, Value::val(this.area.start % (1 << mcu.pc_bits), mcu.pc_bits + 1)?, guard.clone()));
        }
        &mut None => { /* do nothing */ }
    }

    *skip = None;
    Ok(())
}

pub fn match_opcode(reg: &Region, mut address: u64, mcu: &Mcu, names: &mut Names, segments: &mut Segments, matches: &mut Vec<Match>) -> Result<()> {
    let mut skip = None;

    while let Ok(opcode) = reg.read_integer(address, Endianess::Little, 2) {
        info!("disass @ {:#x}", address);

        let opcode = opcode as u16;
        let maybe_skip = match_skip_opcode(opcode, address, names, segments, mcu, matches)?;

        // 1st, check whether we have a opcode that conditionally skips the next one.
        match maybe_skip {
            Some(next_skip) => {
                let m = matches[next_skip.1].clone();

                complete_skip_match(&mut skip, &m, mcu, matches)?;
                address = m.area.end;
                skip = Some(next_skip);

                // matched skip opcode, continue with next.
                continue;
            }
            None => {
                // 2nd, try whether we have a non-skip opcode.
                let next = reg.read_integer(address + 2, Endianess::Little, 2).ok().map(|x| x as u16);
                let l = matches.len();
                match_simple_opcode(address, opcode, next, names, segments, mcu, matches)?;

                if l < matches.len() {
                    match matches.last().cloned() {
                        Some(m) => {
                            complete_skip_match(&mut skip, &m, mcu, matches)?;
                        }
                        None => { /* no known opcode here :( */ }
                    }
                }

                assert!(skip.is_none());
                break;
            }
        }
    }

    Ok(())
}

fn match_skip_opcode(opcode: u16, address: u64, names: &mut Names, segments: &mut Segments, mcu: &Mcu, matches: &mut Vec<Match>) -> Result<Option<(Guard,usize)>> {
    let maybe = match opcode {
        // SBIC: 1001 1001 AAAAA bbb
        0b1001_1001_0000_0000...0b1001_1001_1111_1111 => {
            let rd = decode_io_register(0b11111_000, opcode, names)?;
            let bit = collect_bits(0b111, opcode) as usize;
            let flag = names.var("f", None, 1)?;
            let stmts = semantic::sbxc(rd, bit, flag, names, segments, mcu)?;

            Some(skip("sbic", address, rd.into(), Value::val(bit as u64, 3)?, flag, stmts, mcu)?)
        }
        // SBIS: 1001 1011 AAAAA bbb
        0b1001_1011_0000_0000...0b1001_1011_1111_1111 => {
            let rd = decode_io_register(0b11111_000, opcode, names)?;
            let bit = collect_bits(0b111, opcode) as usize;
            let flag = names.var("f", None, 1)?;
            let stmts = semantic::sbxs(rd, bit, flag, names, segments, mcu)?;

            Some(skip("sbis", address, rd.into(), Value::val(bit as u64, 3)?, flag, stmts, mcu)?)
        }
        // SBRC: 1111 110 rrrrr 0 bbb
        0b1111_1100_0000_0000...0b1111_1101_1111_0111 if opcode & 0b1000 == 0b0000 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let bit = collect_bits(0b111, opcode) as usize;
            let flag = names.var("f", None, 1)?;
            let stmts = semantic::sbxc(rd, bit, flag, names, segments, mcu)?;

            Some(skip("sbrc", address, rd.into(), Value::val(bit as u64, 3)?, flag, stmts, mcu)?)
        }
        // SBRS: 1111 111 rrrrr 0 bbb
        0b1111_1110_0000_0000...0b1111_1111_1111_0111 if opcode & 0b1000 == 0b0000 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let bit = collect_bits(0b111, opcode) as usize;
            let flag = names.var("f", None, 1)?;
            let stmts = semantic::sbxs(rd, bit, flag, names, segments, mcu)?;

            Some(skip("sbrs", address, rd.into(), Value::val(bit as u64, 3)?, flag, stmts, mcu)?)
        }
        // CPSE: 0001 00 R DDDDD RRRR
        0b0001_00_0_00000_0000...0b0001_00_1_11111_1111 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let rr = decode_register(0b1_00000_1111, opcode, names)?;
            let flag = names.var("f", None, 1)?;
            let stmts = semantic::cpse(rd, rr, flag, names, segments, mcu)?;

            Some(skip("cpse", address, rd.into(), rr.into(), flag, stmts, mcu)?)
        }
        _ => None,
    };

    match maybe {
        Some((g,m)) => {
            let p = matches.len();

            matches.push(m);
            Ok(Some((g,p)))
        }
        None => Ok(None)
    }
}

fn match_simple_opcode(address: u64, opcode: u16, next: Option<u16>, names: &mut Names, segs: &mut Segments, mcu: &Mcu, matches: &mut Vec<Match>) -> Result<()> {
    let m = match opcode {
        // ADC: 000111 R DDDDD RRRR
        0b000111_0_00000_0000...0b000111_1_11111_1111 => {
            let r = decode_register(0b0000001000001111, opcode, names)?;
            let d = decode_register(0b0000000111110000, opcode, names)?;
            let stmts = semantic::adc(d, r.into(), names, segs, mcu)?;

            binary("adc", d.into(), r.into(), address, stmts, mcu)?
        }
        // ADD: 0000 11 R DDDDD RRRR
        0b000011_0000000000...0b00011_1111111111 => {
            let r = decode_register(0b000001000001111, opcode, names)?;
            let d = decode_register(0b000000111110000, opcode, names)?;
            let stmts = semantic::add(d, r.into(), names, segs, mcu)?;

            binary("add", d.into(), r.into(), address, stmts, mcu)?
        }
        // ADIW: 10010110 KK DD KKKK
        0b10010110_00000000...0b1001011011111111 => {
            let d = collect_bits(0b0000000000110000, opcode);
            let rd1 = decode_register(0xff, d * 2 + 24, names)?;
            let rd2 = decode_register(0xff, d * 2 + 25, names)?;
            let k = Value::val(collect_bits(0b0000000011001111, opcode) as u64, 8)?;
            let stmts = semantic::adiw(rd1, rd2, k, names, segs, mcu)?;

            mnemonic("adiw", "{u:8}, {u:8}", vec![rd1.into(), k], address..(address + 2), stmts, mcu)?
        }
        // AND: 001000 R DDDDD RRRR
        0b001000_0_00000_0000...0b001000_1_11111_1111 => {
            let r = decode_register(0b0000001000001111, opcode, names)?;
            let d = decode_register(0b0000000111110000, opcode, names)?;
            let stmts = semantic::and(d, r.into(), names, segs, mcu)?;

            binary("and", d.into(), r.into(), address, stmts, mcu)?
        }
        // ANDI: 0111 KKKK DDDD KKKK
        0b0111_0000_0000_0000...0b0111_1111_1111_1111 => {
            let d = collect_bits(0b1111_0000, opcode);
            let d = decode_register(0b11111, d + 16, names)?;
            let k = Value::val(collect_bits(0b0000111100001111, opcode) as u64, 8)?;
            let stmts = semantic::and(d, k.into(), names, segs, mcu)?;

            binary("andi", d.into(), k.into(), address, stmts, mcu)?
        }
        // BR*: 11110 P KKKKKKK FFF
        0b11110_0_0000000_000...0b11110_1_1111111_111 => {
            let k = collect_bits(0b0000001111111000, opcode) as i64;
            match opcode & 0b1_0000000_111 {
                0b0_0000000_000 => branch("brlo", address, names.var("C", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_000 => branch("brsh", address, names.var("C", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_001 => branch("breq", address, names.var("Z", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_001 => branch("brne", address, names.var("Z", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_010 => branch("brmi", address, names.var("N", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_010 => branch("brpl", address, names.var("N", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_011 => branch("brvs", address, names.var("V", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_011 => branch("brvc", address, names.var("V", None, 1)?, false, k, names, mcu)?,
                0b1_0000000_100 => branch("brge", address, names.var("S", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_100 => branch("brlt", address, names.var("S", None, 1)?, true, k, names, mcu)?,
                0b0_0000000_101 => branch("brhs", address, names.var("H", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_101 => branch("brhc", address, names.var("H", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_110 => branch("brts", address, names.var("T", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_110 => branch("brtc", address, names.var("T", None, 1)?, false, k, names, mcu)?,
                0b0_0000000_111 => branch("brie", address, names.var("I", None, 1)?, true, k, names, mcu)?,
                0b1_0000000_111 => branch("brid", address, names.var("I", None, 1)?, false, k, names, mcu)?,
                _ => unreachable!(),
            }
        }
        // BLD: 1111100 DDDDD 0 BBB
        0b1111100_00000_0_000...0b1111100_11111_0_111 if opcode & 0b1000 == 0 => {
            let d = decode_register(0b111110000, opcode, names)?;
            let b = collect_bits(0b111, opcode) as u64;
            let stmts = semantic::bld(d, b, names, segs, mcu)?;

            binary("bld", d.into(), Value::val(b, 3)?, address, stmts, mcu)?
        }
        0x9598 => {
            nonary("break", address, vec![], mcu)?
        }
        // BST: 1111101 DDDDD 0 BBB
        0b1111101_00000_0_000...0b1111101_11111_0_111 if opcode & 0b1000 == 0 => {
            let d = decode_register(0b0000000111110000, opcode, names)?;
            let b = collect_bits(0b111, opcode) as u64;
            let stmts = semantic::bst(d.into(), b, names, segs, mcu)?;

            binary("bst", d.into(), Value::val(b, 3)?, address, stmts, mcu)?
        }
        // CALL: 1001010 KKKKK 111 K  KKKKKKKKKKKKKKKK
        0b1001010_00000_111_0...0b1001010_11111_111_1 if opcode & 0b1110 == 0b1110 => {
            if let Some(k_lo) = next {
                let k = ((collect_bits(0b111110001, opcode) as u64) << 16) | (k_lo as u64);
                let k = Value::val(k * 2, mcu.pc_bits + 1)?;
                let stmts = semantic::call(k, names, segs, mcu)?;

                mnemonic("call", "{c:flash}", vec![k], address..address + 4, stmts, mcu)?
            } else {
                return Ok(());
            }
        }
        // CBI: 10011000 AAAAA BBB
        0b10011000_00000_000...0b10011000_11111_111 => {
            let a = decode_io_register(0b0000000011111000, opcode, names)?;
            let b = (opcode & 0b111) as u64;
            let stmts = semantic::clx(a.into(), b, names, segs, mcu)?;

            binary("cbi", a.into(), Value::val(b, 3)?, address, stmts, mcu)?
        }
        // CLC
        0x9488 => {
            let d = names.var("C", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("clc", address, stmts, mcu)?
        }
        // CLH
        0x94d8 => {
            let d = names.var("H", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("clh", address, stmts, mcu)?
        }
        // CLI
        0x94f8 => {
            let d = names.var("I", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("cli", address, stmts, mcu)?
        }
        // CLN
        0x94a8 => {
            let d = names.var("N", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("cln", address, stmts, mcu)?
        }
        // CLS
        0x94c8 => {
            let d = names.var("S", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("cls", address, stmts, mcu)?
        }
        // CLT
        0x94e8 => {
            let d = names.var("T", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("clt", address, stmts, mcu)?
        }
        // CLV
        0x94b8 => {
            let d = names.var("V", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("clv", address, stmts, mcu)?
        }
        // CLZ
        0x9498 => {
            let d = names.var("Z", None, 1)?;
            let stmts = semantic::clx(d, 0, names, segs, mcu)?;

            nonary("clz", address, stmts, mcu)?
        }
        // COM: 1001010 DDDDD 0000
        0b1001010_00000_0000...0b1001010_11111_0000 if opcode & 0b1111 == 0 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::com(d, names, segs, mcu)?;

            unary("com", d.into(), address, stmts, mcu)?
        }
        // CP: 000101 R DDDDD RRRR
        0b000101_0_00000_0000...0b000101_1_11111_1111 => {
            let d = decode_register(0b111110000, opcode, names)?;
            let r = decode_register(0b1000001111, opcode, names)?;
            let stmts = semantic::cp(d, r.into(), names, segs, mcu)?;

            binary("cp", d.into(), r.into(), address, stmts, mcu)?
        }
        // CPI: 0011 KKKK DDDD KKKK
        0b0011_0000_0000_0000...0b0011_1111_1111_1111 => {
            let d = collect_bits(0b11110000, opcode);
            let d = decode_register(0b11111, d + 16, names)?;
            let k = Value::val(collect_bits(0b111100001111, opcode) as u64, 8)?;
            let stmts = semantic::cp(d, k, names, segs, mcu)?;

            binary("cpi", d.into(), k, address, stmts, mcu)?
        }
        // CPC: 000001 R DDDDD RRRR
        0b000001_0_00000_0000...0b000001_1_11111_1111 => {
            let d = decode_register(0b111110000, opcode, names)?;
            let r = decode_register(0b1000001111, opcode, names)?;
            let stmts = semantic::cpc(d, r.into(), names, segs, mcu)?;

            binary("cpc", d.into(), r.into(), address, stmts, mcu)?
        }
        // DEC: 1001010 DDDDD 1010
        0b1001010_00000_1010...0b1001010_11111_1010 if opcode & 0b1111 == 0b1010 => {
            let d = decode_register(0b111110000, opcode, names)?;
            let stmts = semantic::dec(d, names, segs, mcu)?;

            unary("dec", d.into(), address, stmts, mcu)?
        }
        // DES: 10010100 KKKK 1011
        0b1001010_00000_1011...0b1001010_11111_1011 if opcode & 0b1111 == 0b1011 => {
            let k = collect_bits(0b111110000, opcode) as u64;
            let stmts = semantic::des(names, segs, mcu)?;

            unary("des", Value::val(k as u64, 6)?, address, stmts, mcu)?
        }
        // EICALL: 1001 0101 0001 1001
        0b1001010100011001 => {
            let stmts = semantic::eicall(names, segs, mcu)?;

            nonary("eicall", address, stmts, mcu)?
        }
        // EIJMP: 1001 0100 0001 1001
        0b1001_0100_0001_1001 => {
            let tgt = names.var("q", None, mcu.pc_bits)?;
            let ptr = names.var("Z", None, 24)?;
            let lo = names.var("R30", None, 8)?;
            let hi = names.var("R31", None, 8)?;
            let rampz = names.var("RAMPZ", None, 8)?;
            let stmts_load = semantic::load_wide(ptr, hi, lo, Some(rampz), names, segs, mcu)?;
            let stmts = semantic::eijmp(ptr, tgt, names, segs, mcu)?;

            matches.push(shadow("__load_z", address, stmts_load, mcu)?);
            matches.push(jump("eijmp", tgt.into(), None, address, stmts)?);
            return Ok(());
        }
        // ELPM (1): 1001 0101 1101 1000
        // ELPM (2): 1001 000 DDDDD 0110
        // ELPM (3): 1001 000 DDDDD 0111
          0b1001_0101_1101_1000
        | 0b1001_000_00000_0110...0b1001_000_11111_0110
        | 0b1001_000_00000_0111...0b1001_000_11111_0111
        if opcode & 0b111_00000_1111 == 0b010_00000_1000
        || opcode & 0b111_00000_1111 == 0b000_00000_0110
        || opcode & 0b111_00000_1111 == 0b000_00000_0111 => {
            let lo = names.var("R30", None, 8)?;
            let hi = names.var("R31", None, 8)?;
            let rampz = names.var("RAMPZ", None, 8)?;
            let rd = if opcode & 0b1000 == 0b1000 {
                names.var("R0", None, 8)?
            } else {
                decode_register(0b11111_0000, opcode, names)?
            };
            let inc = opcode & 0b0111 == 0b0111;
            let ptr = if inc {
                names.var("Z+", None, 24)?
            } else {
                names.var("Z", None, 24)?
            };
            let stmts_load = semantic::load_wide(ptr, hi, lo, Some(rampz), names, segs, mcu)?;
            let stmts = semantic::elpm(rd, ptr, inc, names, segs, mcu)?;

            matches.push(shadow("__load_z", address, stmts_load, mcu)?);

            if opcode == 0b1001_0101_1101_1000 {
                matches.push(nonary("elpm", address, stmts, mcu)?);
            } else {
                matches.push(binary("elpm", rd.into(), ptr.into(), address, stmts, mcu)?);
            }

            if inc {
                let stmts_store = semantic::store_wide(ptr, hi, lo, Some(rampz), names, segs, mcu)?;
                matches.push(shadow("__store_z", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // EOR: 001001 R DDDDD RRRR
        0b001001_0_00000_0000...0b001001_1_11111_1111 => {
            let r = decode_register(0b1_00000_1111, opcode, names)?;
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::eor(d, r.into(), names, segs, mcu)?;

            binary("eor", d.into(), r.into(), address, stmts, mcu)?
        }
        // FMUL: 0000 0011 0 DDD 1 RRR
        0b0000_00110_000_1_000...0b0000_00110_111_1_111 if opcode & 0b1000 == 0b1000 => {
            let r = collect_bits(0b111, opcode);
            let d = collect_bits(0b111_0000, opcode);
            let rd = decode_register(0b11111, d + 16, names)?;
            let rr = decode_register(0b11111, r + 16, names)?;
            let stmts = semantic::fmul(rd, rr.into(), names, segs, mcu)?;

            binary("fmul", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // FMULS: 0000 0011 1 DDD 0 RRR
        0b0000_0011_1_000_0_000...0b0000_0011_1_111_0_111 if opcode & 0b1000 == 0 => {
            let r = collect_bits(0b111, opcode);
            let d = collect_bits(0b111_0000, opcode);
            let rd = decode_register(0b11111, d + 16, names)?;
            let rr = decode_register(0b11111, r + 16, names)?;
            let stmts = semantic::fmuls(rd, rr.into(), names, segs, mcu)?;

            binary("fmuls", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // FMUL: 0000 0011 1 DDD 1 RRR
        0b0000_00111_000_1_000...0b0000_00111_111_1_111 if opcode & 0b1000 == 0b1000 => {
            let r = collect_bits(0b111, opcode);
            let d = collect_bits(0b111_0000, opcode);
            let rd = decode_register(0b11111, d + 16, names)?;
            let rr = decode_register(0b11111, r + 16, names)?;
            let stmts = semantic::fmulsu(rd, rr.into(), names, segs, mcu)?;

            binary("fmulsu", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // ICALL: 1001 0101 0000 1001
        0b1001010100001001 => {
            let stmts = semantic::icall(names, segs, mcu)?;

            nonary("icall", address, stmts, mcu)?
        }
        // IJMP: 0x9409
        0x9409 => {
            let tgt = names.var("q", None, mcu.pc_bits + 1)?;
            let stmts = semantic::ijmp(tgt, names, segs, mcu)?;
            let m = Match{
                area: address..(address + 2),
                opcode:"ijmp".into(),
                operands: SmallVec::default(),
                instructions: stmts,
                jumps: SmallVec::from_vec(vec![
                    (address, tgt.into(), Guard::True)
                ]),
            };
            matches.push(m);
            return Ok(());
        }
        // IN: 10110 AA DDDDD AAAA
        0b10110_00_00000_0000...0b10110_11_11111_1111 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let a = Value::val(collect_bits(0b11_00000_1111, opcode) as u64, 8)?;
            let stmts = semantic::in_(rd, a, names, segs, mcu)?;

            binary("in", rd.into(), a, address, stmts, mcu)?
        }
        // JMP: 1001 010 KKKKK 110 K  KKKK KKKK KKKK KKKK
        0b1001_010_00000_110_0...0b1001_010_11111_110_1
            if next.is_some()
            && opcode & 0b1111_111_00000_111_0 == 0b1001_010_00000_110_0
        => {
            let k1 = collect_bits(0b11111_000_1, opcode) as u64 * 2;
            let k2 = next.unwrap() as u64 * 2;
            let k = Value::val((k2 + (k1 << 16)) % (1 << mcu.pc_bits), mcu.pc_bits + 1)?;

            matches.push(jump("jmp", k, None, address, vec![])?);
            return Ok(());
        }
        // INC: 1001010 DDDDD 0011
        0b1001010_00000_0011...0b1001010_11111_0011 if opcode & 0b1111 == 0b0011 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::inc(rd, names, segs, mcu)?;

            unary("inc", rd.into(), address, stmts, mcu)?
        }
        // LAC: 1001 001 RRRRR 0110
        0b1001_001_00000_0110...0b1001_001_11111_0110
            if opcode & 0b1111111_00000_1111 == 0b1001001_00000_0110
        => {
            let rr = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::lac(rr, names, segs, mcu)?;

            unary("lac", rr.into(), address, stmts, mcu)?
        }
        // LAS: 1001 001 RRRRR 0101
        0b1001_001_00000_0101...0b1001_001_11111_0101
            if opcode & 0b1111111_00000_1111 == 0b1001001_00000_0101
        => {
            let rr = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::las(rr, names, segs, mcu)?;

            unary("las", rr.into(), address, stmts, mcu)?
        }
        // LAT: 1001 001 RRRRR 0111
        0b1001_001_00000_0111...0b1001_001_11111_0111
            if opcode & 0b1111111_00000_1111 == 0b1001001_00000_0111
        => {
            let rr = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::lat(rr, names, segs, mcu)?;

            unary("lat", rr.into(), address, stmts, mcu)?
        }
        // LD X: 1001 000 DDDDD 1100
        //       1001 000 DDDDD 1110
        //       1001 000 DDDDD 1101
          0b1001_000_00000_1100...0b1001_000_11111_1100
        | 0b1001_000_00000_1110...0b1001_000_11111_1110
        | 0b1001_000_00000_1101...0b1001_000_11111_1101
            if opcode & 0b1111 == 0b1100 || opcode & 0b1111 == 0b1110 || opcode & 0b1111 == 0b1101
        => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (x,off) = match opcode & 0b111 {
                0b100 => (names.var("X", None, 16)?,AddressOffset::None),
                0b110 => (names.var("-X", None, 16)?,AddressOffset::Predecrement),
                0b101 => (names.var("X+", None, 16)?,AddressOffset::Postincrement),
                _ => unreachable!(),
            };
            let lo = names.var("R26", None, 8)?;
            let hi = names.var("R27", None, 8)?;
            let stmts_load = semantic::load_wide(x, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::ld(x, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_x", address, stmts_load, mcu)?);
            matches.push(binary("ld", rd.into(), x.into(), address, stmts, mcu)?);

            if off != AddressOffset::None {
                let stmts_store = semantic::store_wide(x, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_x", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // LD Y: 1000 000 RRRRR 1000
        //       1001 000 RRRRR 1001
        //       1001 000 RRRRR 1010
        //       10 Q 0 QQ 0 RRRRR 1 QQQ
          0b1000_000_00000_1000...0b1000_000_11111_1000
        | 0b1001_000_00000_1001...0b1001_000_11111_1001
        | 0b1001_000_00000_1010...0b1001_000_11111_1010
        | 0b10_0_0_00_0_00000_1_000...0b10_1_0_11_0_11111_1_111
            if opcode & 0b1111_111_00000_1111 == 0b1000_000_00000_1000
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_1001
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_1010
            || opcode & 0b11_0_1_00_1_00000_1_000 == 0b10_0_0_00_0_00000_1_000
        => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (y,off) = match opcode & 0b1_000000000_111 {
                0b1_000000000_001 => (names.var("Y+", None, 16)?,AddressOffset::Postincrement),
                0b1_000000000_010 => (names.var("-Y", None, 16)?,AddressOffset::Predecrement),
                0b0_000000000_000...0b0_000000000_111 => {
                    match collect_bits(0b1_0_11_0_00000_0_111, opcode) {
                        0 => (names.var("Y", None, 16)?,AddressOffset::None),
                        q => (names.var(format!("Y+{}", q), None, 16)?,AddressOffset::Displacement(q as u8))
                    }
                }
                _ => unreachable!(),
            };
            let opcode = match off {
                AddressOffset::Displacement(_) => "ldd",
                _ => "ld",
            };
            let lo = names.var("R28", None, 8)?;
            let hi = names.var("R29", None, 8)?;
            let stmts_load = semantic::load_wide(y, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::ld(y, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_y", address, stmts_load, mcu)?);
            matches.push(binary(opcode, rd.into(), y.into(), address, stmts, mcu)?);

            if off == AddressOffset::Predecrement || off == AddressOffset::Postincrement {
                let stmts_store = semantic::store_wide(y, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_y", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // LD Z: 1000 000 RRRRR 0000
        //       1001 000 RRRRR 0001
        //       1001 000 RRRRR 0010
        //       10 Q 0 QQ 0 RRRRR 0 QQQ
          0b1000_000_00000_0000...0b1000_000_11111_0000
        | 0b1001_000_00000_0001...0b1001_000_11111_0001
        | 0b1001_000_00000_0010...0b1001_000_11111_0010
        | 0b10_0_0_00_0_00000_0_000...0b10_1_0_11_0_11111_0_111
            if opcode & 0b1111_111_00000_1111 == 0b1000_000_00000_0000
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_0001
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_0010
            || opcode & 0b11_0_1_00_1_00000_1_000 == 0b10_0_0_00_0_00000_0_000
         => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (z,off) = match opcode & 0b1_000000000_111 {
                0b1_000000000_001 => (names.var("Z+", None, 16)?,AddressOffset::Postincrement),
                0b1_000000000_010 => (names.var("-Z", None, 16)?,AddressOffset::Predecrement),
                0b0_000000000_000...0b0_000000000_111 => {
                    match collect_bits(0b1_0_11_0_00000_0_111, opcode) {
                        0 => (names.var("Z", None, 16)?,AddressOffset::None),
                        q => (names.var(format!("Z+{}", q), None, 16)?,AddressOffset::Displacement(q as u8))
                    }
                }
                _ => unreachable!(),
            };
            let opcode = match off {
                AddressOffset::Displacement(_) => "ldd",
                _ => "ld",
            };
            let lo = names.var("R30", None, 8)?;
            let hi = names.var("R31", None, 8)?;
            let stmts_load = semantic::load_wide(z, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::ld(z, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_z", address, stmts_load, mcu)?);
            matches.push(binary(opcode, rd.into(), z.into(), address, stmts, mcu)?);

            if off == AddressOffset::Predecrement || off == AddressOffset::Postincrement {
                let stmts_store = semantic::store_wide(z, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_z", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // LDI: 1110 KKKK DDDD KKKK
        0b1110_0000_0000_0000...0b1110_1111_1111_1111 => {
            let d = collect_bits(0b1111_0000, opcode) + 16;
            let rd = decode_register(0b11111, d, names)?;
            let k = collect_bits(0b1111_0000_1111, opcode) as u64;
            let stmts = semantic::ldi(rd, k, names, segs, mcu)?;

            binary("ldi", rd.into(), Value::val(k, 8)?, address, stmts, mcu)?
        }
        // LDS: 1001 000 DDDDD 0000  KKKK KKKK KKKK KKKK
        0b1001_000_00000_0000...0b1001_000_11111_0000
            if opcode & 0b1111 == 0
            && next.is_some()
        => {
            let k = next.unwrap() as u64;
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let k = Value::val(k, mcu.pc_bits + 1)?;
            let stmts = semantic::lds(rd, k, names, segs, mcu)?;

            mnemonic("lds", "{8}, {c:flash}", vec![rd.into(), k], address..address + 4, stmts, mcu)?
        }
        /*
        // LDS: 1010 0 KKK DDDD KKKK
        0b1010_0_000_0000_0000...0b1010_0_111_1111_1111 => {
            let rd = decode_register(0b1111_0000, opcode, names)?;
            let k = collect_bits(0b111_0000_1111, opcode) as u64;
            let k = Value::val(k, mcu.pc_bits + 1)?;
            let stmts = semantic::lds(rd, k, names, segs, mcu)?;

            mnemonic("lds", "{8}, {c:flash}", vec![rd.into(), k], address..address + 2, stmts, mcu)?
        }
        */
        // LPM Z: 1001 0101 1100 1000
        //        1001 000 DDDDD 0100
        //        1001 000 DDDDD 0101
          0b1001_0101_1100_1000
        | 0b1001_000_00000_0100...0b1001_000_11111_0100
        | 0b1001_000_00000_0101...0b1001_000_11111_0101
            if opcode == 0b1001_0101_1100_1000
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_0100
            || opcode & 0b1111_111_00000_1111 == 0b1001_000_00000_0101
        => {
             let rd = match opcode & 0b1111 {
                0b1000 => names.var("R0", None, 8)?,
                0b0100 | 0b0101 => decode_register(0b11111_0000, opcode, names)?,
                _ => unreachable!(),
            };
            let (z,inc) = match opcode & 0b1111 {
                0b1000 => (names.var("Z", None, 16)?,false),
                0b0100 => (names.var("Z", None, 16)?,false),
                0b0101 => (names.var("Z+", None, 16)?,true),
                _ => unreachable!(),
            };
            let lo = names.var("R30", None, 8)?;
            let hi = names.var("R31", None, 8)?;
            let stmts_load = semantic::load_wide(z, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::elpm(z, rd, inc, names, segs, mcu)?;

            matches.push(shadow("__load_z", address, stmts_load, mcu)?);

            if opcode == 0b1001_0101_1100_1000 {
                matches.push(nonary("lpm", address, stmts, mcu)?);
            } else {
                matches.push(binary("lpm", rd.into(), z.into(), address, stmts, mcu)?);
            }

            if inc {
                let stmts_store = semantic::store_wide(z, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_z", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // LSR: 1001010 DDDDD 0110
        0b1001010_00000_0110...0b1001010_11111_0110 if opcode & 0b1111 == 0b0110 => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::lsr(rd, names, segs, mcu)?;

            unary("lsr", rd.into(), address, stmts, mcu)?
        }
        // MOV: 001011 R DDDDD RRRR
        0b001011_0_00000_0000...0b001011_1_11111_1111 => {
            let rr = decode_register(0b1_00000_1111, opcode, names)?;
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::mov(rd, rr.into(), names, segs, mcu)?;

            binary("mov", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // MOVW: 0000 0001 DDDD RRRR
        0b0000_0001_0000_0000...0b0000_0001_1111_1111 => {
            let r = collect_bits(0b1111, opcode);
            let d = collect_bits(0b1111_0000, opcode);
            let rr1 = decode_register(0b11111, r * 2, names)?;
            let rr2 = decode_register(0b11111, r * 2 + 1, names)?;
            let rd1 = decode_register(0b11111, d * 2, names)?;
            let rd2 = decode_register(0b11111, d * 2 + 1, names)?;
            let stmts = semantic::movw(rd1, rd2, rr1, rr2, names, segs, mcu)?;

            binary("movw", rd1.into(), rr1.into(), address, stmts, mcu)?
        }
        // MUL: 1001 11 R DDDDD RRRR
        0b1001_11_0_00000_0000...0b1001_11_1_11111_1111 => {
            let rr = decode_register(0b1_00000_1111, opcode, names)?;
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::mul(rd, rr.into(), names, segs, mcu)?;

            binary("mul", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // MULS: 0000 0010 DDDDD RRRRR
        0b0000_0010_0000_0000...0b0000_0010_1111_1111 => {
            let r = collect_bits(0b1111, opcode);
            let d = collect_bits(0b1111_0000, opcode);
            let rd = decode_register(0b11111, d + 16, names)?;
            let rr = decode_register(0b11111, r + 16, names)?;
            let stmts = semantic::muls(rd, rr.into(), names, segs, mcu)?;

            binary("muls", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // MULSU: 0000 0011 0 DDD 0 RRR
        0b000000110_000_0_000...0b000000110_111_0_111 if opcode & 0b1000 == 0 => {
            let r = collect_bits(0b111, opcode);
            let d = collect_bits(0b111_0000, opcode);
            let rd = decode_register(0b11111, d + 16, names)?;
            let rr = decode_register(0b11111, r + 16, names)?;
            let stmts = semantic::mulsu(rd, rr.into(), names, segs, mcu)?;

            binary("mulsu", rd.into(), rr.into(), address, stmts, mcu)?
        }
        // NEG: 1001010 DDDDD 0001
        0b1001010_00000_0001...0b1001010_11111_0001 if opcode & 0b1111 == 1 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::neg(d, names, segs, mcu)?;

            unary("neg", d.into(), address, stmts, mcu)?
        }
        // ASR: 1001010 DDDDD 0101
        0b1001010_00000_0101...0b1001010_11111_0101 if opcode & 0b1111 == 0b0101 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::asr(d, names, segs, mcu)?;

            unary("asr", d.into(), address, stmts, mcu)?
        }
        // NOP: 0000000000000000
        0 => {
            nonary("nop", address, vec![], mcu)?
        }
        // OR: 001010 R DDDDD RRRR
        0b001010_0_00000_0000...0b001010_1_11111_1111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let r = decode_register(0b1_00000_1111, opcode, names)?;
            let stmts = semantic::or(d, r.into(), names, segs, mcu)?;

            binary("or", d.into(), r.into(), address, stmts, mcu)?
        }
        // ORI: 0110 KKKK DDDD KKKK
        0b0110_0000_0000_0000...0b0110_1111_1111_1111 => {
            let d = collect_bits(0b1111_0000, opcode);
            let d = decode_register(0b11111, d + 16, names)?;
            let k = Value::val(collect_bits(0b1111_0000_1111, opcode) as u64, 8)?;
            let stmts = semantic::or(d, k, names, segs, mcu)?;

            binary("ori", d.into(), k, address, stmts, mcu)?
        }
        // OUT: 10111 AA RRRRR AAAA
        0b10111_00_00000_0000...0b10111_11_11111_1111 => {
            let rr = decode_register(0b11111_0000, opcode, names)?;
            let a = Value::val(collect_bits(0b11_00000_1111, opcode) as u64, 8)?;
            let stmts = semantic::out(rr, a, names, segs, mcu)?;

            binary("out", a, rr.into(), address, stmts, mcu)?
        }
        // POP: 1001000 DDDDD 1111
        0b1001000_00000_1111...0b1001000_11111_1111 if opcode & 0b1111 == 0b1111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::pop(d, names, segs, mcu)?;

            unary("pop", d.into(), address, stmts, mcu)?
        }
        // PUSH: 1001001 DDDDD 1111
        0b1001001_00000_1111...0b1001001_11111_1111 if opcode & 0b1111 == 0b1111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::push(d, names, segs, mcu)?;

            unary("push", d.into(), address, stmts, mcu)?
        }
        // RCALL: 1101 KKKKKKKKKKKK
        0b1101_000000000000...0b1101_111111111111 => {
            let k = Value::val(collect_bits(0b11_00000_1111, opcode) as u64 * 2 + address, mcu.pc_bits + 1)?;
            let stmts = semantic::rcall(k, names, segs, mcu)?;

            unary("rcall", k, address, stmts, mcu)?
        }
        // RET: 1001 0101 0000 1000
        0b1001_0101_0000_1000 => {
            let stmts = semantic::ret(names, segs, mcu)?;

            nonary("ret", address, stmts, mcu)?
        }
        // RETI: 1001 0101 0001 1000
        0b1001_0101_0001_1000 => {
            let stmts = semantic::ret(names, segs, mcu)?;

            nonary("reti", address, stmts, mcu)?
        }
        // RJMP: 1100 KKKK KKKK KKKK
        0b1100_0000_0000_0000...0b1100_1111_1111_1111 => {
            let k = collect_bits(0b1111_1111_1111, opcode) as u64 * 2 + address + 2;
            let k = Value::val(k % (1 << mcu.pc_bits + 1), mcu.pc_bits + 1)?;
            let v = collect_bits(0b1111_1111_1111, opcode) as i64;
            let v = if v >= 0b1000_0000_0000 {
                v - 0b1_0000_0000_0000
            } else {
                v
            } * 2;
            let v = names.var(format!(".{:+}", v), None, mcu.pc_bits + 1)?;

            matches.push(jump("rjmp", v.into(), Some(k), address, vec![])?);
            return Ok(());
        }
        // ROR: 1001010 DDDDD 0111
        0b1001010_00000_0111...0b1001010_11111_0111 if opcode & 0b1111 == 0b0111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::ror(d, names, segs, mcu)?;

            unary("ror", d.into(), address, stmts, mcu)?
        }
        // SBC: 000010 R DDDDD RRRR
        0b000010_0_00000_0000...0b000010_1_11111_1111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let r = decode_register(0b1_00000_1111, opcode, names)?;
            let stmts = semantic::sbc(d, r.into(), names, segs, mcu)?;

            binary("sbc", d.into(), r.into(), address, stmts, mcu)?
        }
        // SBCI: 0100 KKKK DDDD KKKK
        0b0100_0000_0000_0000...0b0100_1111_1111_1111 => {
            let d = collect_bits(0b1111_0000, opcode);
            let d = decode_register(0b11111, d + 16, names)?;
            let k = Value::val(collect_bits(0b1111_0000_1111, opcode) as u64, 8)?;
            let stmts = semantic::sbc(d, k, names, segs, mcu)?;

            binary("sbci", d.into(), k, address, stmts, mcu)?
        }
        // SBI: 10011010 AAAAA BBB
        0b10011010_00000_000...0b10011010_11111_111 => {
            let a = decode_io_register(0b0000000011111000, opcode, names)?;
            let b = (opcode & 0b111) as u64;
            let stmts = semantic::sbi(a.into(), b, names, segs, mcu)?;

            binary("sbi", a.into(), Value::val(b, 3)?, address, stmts, mcu)?
        }
        // SBIW: 1001 0111 KK DD KKKK
        0b1001_0111_00000000...0b1001_0111_11111111 => {
            let d = collect_bits(0b0000000000110000, opcode);
            let rd1 = decode_register(0xff, d * 2 + 24, names)?;
            let rd2 = decode_register(0xff, d * 2 + 25, names)?;
            let k = Value::val(collect_bits(0b0000000011001111, opcode) as u64, 8)?;
            let stmts = semantic::sbiw(rd1, rd2, k, names, segs, mcu)?;

            mnemonic("sbiw", "{u:8}, {u:8}", vec![rd1.into(), k], address..(address + 2), stmts, mcu)?
        }
        // ST X: 1001 001 RRRRR 1100
        //       1001 001 RRRRR 1110
        //       1001 001 RRRRR 1101
          0b1001_001_00000_1100...0b1001_001_11111_1100
        | 0b1001_001_00000_1110...0b1001_001_11111_1110
        | 0b1001_001_00000_1101...0b1001_001_11111_1101
            if opcode & 0b1111 == 0b1100
            || opcode & 0b1111 == 0b1110
            || opcode & 0b1111 == 0b1101
        => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (x,off) = match opcode & 0b111 {
                0b100 => (names.var("X", None, 16)?,AddressOffset::None),
                0b110 => (names.var("-X", None, 16)?,AddressOffset::Predecrement),
                0b101 => (names.var("X+", None, 16)?,AddressOffset::Postincrement),
                _ => unreachable!(),
            };
            let lo = names.var("R26", None, 8)?;
            let hi = names.var("R27", None, 8)?;
            let stmts_load = semantic::load_wide(x, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::st(x, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_x", address, stmts_load, mcu)?);
            matches.push(binary("st", x.into(), rd.into(), address, stmts, mcu)?);

            if off != AddressOffset::None {
                let stmts_store = semantic::store_wide(x, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_x", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // ST Y: 1001 001 RRRRR 1000
        //       1001 001 RRRRR 1001
        //       1001 001 RRRRR 1010
        //       10 Q 0 QQ 1 RRRRR 1 QQQ
          0b1001_001_00000_1000...0b1001_001_11111_1000
        | 0b1001_001_00000_1001...0b1001_001_11111_1001
        | 0b1001_001_00000_1010...0b1001_001_11111_1010
        | 0b10_0_0_00_1_00000_1_000...0b10_1_0_11_1_11111_1_111
            if opcode & 0b1111_111_00000_1111 == 0b1001_001_00000_1000
            || opcode & 0b1111_111_00000_1111 == 0b1001_001_00000_1001
            || opcode & 0b1111_111_00000_1111 == 0b1001_001_00000_1010
            || opcode & 0b11_0_1_00_1_00000_1_000 == 0b10_0_0_00_1_00000_1_000
         => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (y,off) = match opcode & 0b1_000000000_111 {
                0b1_000000000_001 => (names.var("Y+", None, 16)?,AddressOffset::Postincrement),
                0b1_000000000_010 => (names.var("-Y", None, 16)?,AddressOffset::Predecrement),
                0b0_000000000_000...0b0_000000000_111 => {
                    match collect_bits(0b1_0_11_0_00000_0_111, opcode) {
                        0 => (names.var("Y", None, 16)?,AddressOffset::None),
                        q => (names.var(format!("Y+{}", q), None, 16)?,AddressOffset::Displacement(q as u8))
                    }
                }
                _ => unreachable!(),
            };
            let opcode = match off {
                AddressOffset::Displacement(_) => "std",
                _ => "st",
            };
            let lo = names.var("R28", None, 8)?;
            let hi = names.var("R29", None, 8)?;
            let stmts_load = semantic::load_wide(y, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::st(y, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_y", address, stmts_load, mcu)?);
            matches.push(binary(opcode, y.into(), rd.into(), address, stmts, mcu)?);

            if off == AddressOffset::Predecrement || off == AddressOffset::Postincrement {
                let stmts_store = semantic::store_wide(y, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_y", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // ST Z: 1000 001 RRRRR 0000
        //       1001 001 RRRRR 0001
        //       1001 001 RRRRR 0010
        //       10 Q 0 QQ 1 RRRRR 0 QQQ
          0b1000_001_00000_0000...0b1000_001_11111_0000
        | 0b1001_001_00000_0001...0b1001_001_11111_0001
        | 0b1001_001_00000_0010...0b1001_001_11111_0010
        | 0b10_0_0_00_1_00000_0_000...0b10_1_0_11_1_11111_0_111
            if opcode & 0b1111_111_00000_1111 == 0b1000_001_00000_0000
            || opcode & 0b1111_111_00000_1111 == 0b1001_001_00000_0001
            || opcode & 0b1111_111_00000_1111 == 0b1001_001_00000_0010
            || opcode & 0b11_0_1_00_1_00000_1_000 == 0b10_0_0_00_1_00000_0_000
        => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let (z,off) = match opcode & 0b1_000000000_111 {
                0b1_000000000_001 => (names.var("Z+", None, 16)?,AddressOffset::Postincrement),
                0b1_000000000_010 => (names.var("-Z", None, 16)?,AddressOffset::Predecrement),
                0b0_000000000_000...0b0_000000000_111 => {
                    match collect_bits(0b1_0_11_0_00000_0_111, opcode) {
                        0 => (names.var("Z", None, 16)?,AddressOffset::None),
                        q => (names.var(format!("Z+{}", q), None, 16)?,AddressOffset::Displacement(q as u8))
                    }
                }
                _ => unreachable!(),
            };
            let opcode = match off {
                AddressOffset::Displacement(_) => "std",
                _ => "st",
            };
            let lo = names.var("R30", None, 8)?;
            let hi = names.var("R31", None, 8)?;
            let stmts_load = semantic::load_wide(z, hi, lo, None, names, segs, mcu)?;
            let stmts = semantic::st(z, rd, off, names, segs, mcu)?;

            matches.push(shadow("__load_z", address, stmts_load, mcu)?);
            matches.push(binary(opcode, z.into(), rd.into(), address, stmts, mcu)?);

            if off == AddressOffset::Predecrement || off == AddressOffset::Postincrement {
                let stmts_store = semantic::store_wide(z, hi, lo, None, names, segs, mcu)?;
                matches.push(shadow("__store_z", address, stmts_store, mcu)?);
            }

            return Ok(());
        }
        // STS: 1001 001 DDDDD 0000
        0b1001001_00000_0000...0b1001001_11111_0000
            if opcode & 0b1111 == 0 && next.is_some()
        => {
            let rd = decode_register(0b11111_0000, opcode, names)?;
            let k = Value::val(next.unwrap() as u64, 16)?;
            let stmts = semantic::sts(rd, k, names, segs, mcu)?;

            binary("sts", k, rd.into(), address, stmts, mcu)?
        }
        // SUB: 000110 R DDDDD RRRR
        0b000110_0_00000_0000...0b000110_1_11111_1111 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let r = decode_register(0b1_00000_1111, opcode, names)?;
            let stmts = semantic::sub(d, r.into(), names, segs, mcu)?;

            binary("sub", d.into(), r.into(), address, stmts, mcu)?
        }
        // SUBI: 0101 KKKK DDDD KKKK
        0b0101_0000_0000_0000...0b0101_1111_1111_1111 => {
            let d = collect_bits(0b1111_0000, opcode);
            let d = decode_register(0b11111, d + 16, names)?;
            let k = Value::val(collect_bits(0b1111_0000_1111, opcode) as u64, 8)?;
            let stmts = semantic::sub(d, k, names, segs, mcu)?;

            binary("subi", d.into(), k, address, stmts, mcu)?
        }
        // SEC
        0b1001_0100_0000_1000 => {
            let d = names.var("C", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("sec", address, stmts, mcu)?
        }
        // SEH
        0b1001_0100_0101_1000 => {
            let d = names.var("H", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("seh", address, stmts, mcu)?
        }
        // SEI
        0b1001_0100_0111_1000 => {
            let d = names.var("I", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("sei", address, stmts, mcu)?
        }
        // SEN
        0b1001_0100_0010_1000 => {
            let d = names.var("N", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("sen", address, stmts, mcu)?
        }
        // SES
        0b1001_0100_0100_1000 => {
            let d = names.var("S", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("ses", address, stmts, mcu)?
        }
        // SET
        0b1001_0100_0110_1000 => {
            let d = names.var("T", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("set", address, stmts, mcu)?
        }
        // SEV
        0b1001_0100_0011_1000 => {
            let d = names.var("V", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("sev", address, stmts, mcu)?
        }
        // SEZ
        0b1001_0100_0001_1000 => {
            let d = names.var("Z", None, 1)?;
            let stmts = semantic::sex(d, 0, names, segs, mcu)?;

            nonary("sez", address, stmts, mcu)?
        }
        // SLEEP: 1001 0101 1000 1000
        0b1001_0101_1000_1000 => {
            nonary("sleep", address, vec![], mcu)?
        }
        // SPM: 1001 0101 111x 1000
        0b1001_0101_1110_1000 | 0b1001_0101_1111_1000 => {
            nonary("spm", address, vec![], mcu)?
        }
        // SWAP: 1001010 DDDDD 0010
        0b1001010_00000_0010...0b1001010_11111_0010 if opcode & 0b1111 == 0b0010 => {
            let d = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::swap(d, names, segs, mcu)?;

            unary("swap", d.into(), address, stmts, mcu)?
        }
        // WDR: 0x95a8
        0x95a8 => {
            nonary("wdr", address, vec![], mcu)?
        }
        // XCH: 1001001 RRRRR 0100
        0b1001001_00000_0100...0b1001001_11111_0100 if opcode & 0b1111 == 0b0100 => {
            let r = decode_register(0b11111_0000, opcode, names)?;
            let stmts = semantic::xch(names.var("Z", None, 16)?, r, names, segs, mcu)?;

            unary("xch", r.into(), address, stmts, mcu)?
        }

         _ => {
             debug!("failed to match {:0>16b}", opcode);
             return Ok(());
         }
    };

    matches.push(m);
    Ok(())
}
