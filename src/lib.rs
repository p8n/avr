// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! 8-bit AVR disassembler.
//!
//! This disassembler handles the 8-bit AVR microcontroller instruction set including XMEGA.

#![allow(missing_docs)]

#[macro_use]
extern crate log;

#[macro_use]
extern crate p8n_types;
extern crate byteorder;
extern crate smallvec;

mod syntax;
mod semantic;

mod disassembler;
pub use disassembler::{Avr, Mcu};
