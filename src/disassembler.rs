// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::borrow::Cow;

use p8n_types::{
    Architecture,
    Match,
    Region,
    Result,
    Names,
    Segments,
    Strings,
};

use syntax;

#[derive(Clone,Debug)]
pub enum Avr {}

impl Architecture for Avr {
    type Configuration = Mcu;

    fn prepare(_: &Region, cfg: &Mcu) -> Result<Cow<'static, [(&'static str, u64, &'static str)]>> {
        Ok(cfg.int_vec.into())
    }

    fn decode(reg: &Region, start: u64, cfg: &Mcu, names: &mut Names, segments: &mut Segments, _: &mut Strings, matches: &mut Vec<Match>) -> Result<()> {
        syntax::match_opcode(reg, start, cfg, names, segments, matches)
    }
}

#[derive(Clone,Debug)]
pub struct Mcu {
    /// Width of the program counter in bits
    pub pc_bits: usize,
    /// Interrupt vector: (name, offset, comment)
    pub int_vec: &'static [(&'static str, u64, &'static str)],
}

const ATMEGA_103_IV: [(&'static str, u64, &'static str); 24] = [
    ("RESET", 0, "MCU Reset Interrupt"),
    ("INT0", 4, "External Interrupt 0"),
    ("INT1", 8, "External Interrupt 1"),
    ("INT2", 12, "External Interrupt 2"),
    ("INT3", 16, "External Interrupt 3"),
    ("INT4", 20, "External Interrupt 4"),
    ("INT5", 24, "External Interrupt 5"),
    ("INT6", 0x0e, "External Interrupt 6"),
    ("INT7", 0x10, "External Interrupt 7"),
    ("OC2", 0x12, "Timer/Counter2 Compare Match"),
    ("OVF2", 0x14, "Timer/Counter2 Overflow"),
    ("ICP1", 0x16, "Timer/Counter1 Capture Event"),
    ("OC1A", 0x18, "Timer/Counter1 Compare Match A"),
    ("OC1B", 0x1a, "Timer/Counter1 Compare Match B"),
    ("OVF1", 0x1c, "Timer/Counter1 Overflow"),
    ("OC0", 0x1e, "Timer/Counter0 Compare Match"),
    ("OVF0", 0x20, "Timer/Counter0 Overflow"),
    ("SPI", 0x22, "SPI Serial Transfer Complete"),
    ("URXC", 0x24, "UART, Rx Complete"),
    ("UDRE", 0x26, "UART Data Register Empty"),
    ("UTXC", 0x28, "UART, Tx Complete"),
    ("ADCC", 0x2a, "ADC Conversion Complete"),
    ("ERDY", 0x2c, "EEPROM Ready"),
    ("ACI", 0x2e, "Analog Comparator")];

const ATMEGA_16_IV: [(&'static str, u64, &'static str); 21] = [
    ("RESET", 0, "MCU Reset Interrupt"),
    ("INT0", 0x0004, "External Interrupt Request 0"),
    ("INT1", 0x0008, "External Interrupt Request 1"),
    ("OC2", 0x000c, "Timer/Counter2 Compare Match"),
    ("OVF2", 0x0010, "Timer/Counter2 Overflow"),
    ("ICP1", 0x0014, "Timer/Counter1 Capture Event"),
    ("OC1A", 0x0018, "Timer/Counter1 Compare Match A"),
    ("OC1B", 0x001c, "Timer/Counter1 Compare Match B"),
    ("OVF1", 0x0020, "Timer/Counter1 Overflow"),
    ("OVF0", 0x0024, "Timer/Counter0 Overflow"),
    ("SPI", 0x0028, "Serial Transfer Complete"),
    ("URXC", 0x002c, "USART, Rx Complete"),
    ("UDRE", 0x0030, "USART Data Register Empty"),
    ("UTXC", 0x0034, "USART, Tx Complete"),
    ("ADCC", 0x0038, "ADC Conversion Complete"),
    ("ERDY", 0x003c, "EEPROM Ready"),
    ("ACI", 0x0040, "Analog Comparator"),
    ("TWI", 0x0044, "2-wire Serial Interface"),
    ("INT2", 0x0048, "External Interrupt Request 2"),
    ("OC0", 0x004c, "Timer/Counter0 Compare Match"),
    ("SPMR", 0x0050, "Store Program Memory Ready"),
];

impl Mcu {
    pub fn new(pc_bits: usize, iv: &'static [(&'static str, u64, &'static str)]) -> Mcu {
        Mcu {
            pc_bits: pc_bits,
            int_vec: iv,
        }
    }

    pub fn atmega103() -> Mcu {
        Self::new(16,&ATMEGA_103_IV)
    }

    /*
    pub fn atmega8() -> Mcu {
        Self::new(
            0xfff,
            vec![
                ("RESET", 0, "MCU Reset Interrupt"),
                ("INT0", 0x02, "External Interrupt Request 0"),
                ("INT1", 0x04, "External Interrupt Request 1"),
                ("OC2", 0x06, "Timer/Counter2 Compare Match"),
                ("OVF2", 0x08, "Timer/Counter2 Overflow"),
                ("ICP1", 0x0a, "Timer/Counter1 Capture Event"),
                ("OC1A", 0x0c, "Timer/Counter1 Compare Match A"),
                ("OC1B", 0x0e, "Timer/Counter1 Compare Match B"),
                ("OVF1", 0x10, "Timer/Counter1 Overflow"),
                ("OVF0", 0x12, "Timer/Counter0 Overflow"),
                ("SPI", 0x14, "Serial Transfer Complete"),
                ("URXC", 0x16, "USART, Rx Complete"),
                ("UDRE", 0x18, "USART Data Register Empty"),
                ("UTXC", 0x1a, "USART, Tx Complete"),
                ("ADCC", 0x1c, "ADC Conversion Complete"),
                ("ERDY", 0x1e, "EEPROM Ready"),
                ("ACI", 0x20, "Analog Comparator"),
                ("TWI", 0x22, "2-wire Serial Interface"),
                ("SPMR", 0x24, "Store Program Memory Ready"),
            ],
        )
    }

    pub fn atmega88() -> Mcu {
        Self::new(
            0xfff,
            vec![
                ("RESET", 0, "MCU Reset Interrupt"),
                ("INT0", 2, "External Interrupt Request 0"),
                ("INT1", 4, "External Interrupt Request 1"),
                ("PCI0", 6, "Pin Change Interrupt Request 0"),
                ("PCI1", 8, "Pin Change Interrupt Request 1"),
                ("PCI2", 10, "Pin Change Interrupt Request 2"),
                ("WDT", 12, "Watchdog Time-out Interrupt"),
                ("OC2A", 14, "Timer/Counter2 Compare Match A"),
                ("OC2B", 16, "Timer/Counter2 Compare Match B"),
                ("OVF2", 18, "Timer/Counter2 Overflow"),
                ("ICP1", 20, "Timer/Counter1 Capture Event"),
                ("OC1A", 22, "Timer/Counter1 Compare Match A"),
                ("OC1B", 24, "Timer/Counter1 Compare Match B"),
                ("OVF1", 26, "Timer/Counter1 Overflow"),
                ("OC0A", 28, "TimerCounter0 Compare Match A"),
                ("OC0B", 30, "TimerCounter0 Compare Match B"), // XXX: m88def.inc says 0x1f (words)
                ("OVF0", 32, "Timer/Couner0 Overflow"),
                ("SPI", 34, "SPI Serial Transfer Complete"),
                ("URXC", 36, "USART Rx Complete"),
                ("UDRE", 38, "USART, Data Register Empty"),
                ("UTXC", 40, "USART Tx Complete"),
                ("ADCC", 42, "ADC Conversion Complete"),
                ("ERDY", 44, "EEPROM Ready"),
                ("ACI", 46, "Analog Comparator"),
                ("TWI", 48, "Two-wire Serial Interface"),
                ("SPMR", 50, "Store Program Memory Read"),
            ],
        )
    }*/

    pub fn atmega16() -> Mcu {
        Self::new(12,&ATMEGA_16_IV)
    }
}
