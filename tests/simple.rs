// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2015,2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

extern crate panopticon_core;
extern crate panopticon_avr;
extern crate simple_logger;

use std::fs::File;

use panopticon_avr::{Avr, Mcu};
use panopticon_core::neo::{
    Architecture,
    Region,
    Segments,
    Names,
    Value,
};

#[test]
fn jmp_overflow() {
    let _ = simple_logger::init();
    let fd = File::open("tests/data/avr-jmp-overflow.bin").unwrap();
    let reg = Region::from_file("", 22, fd, None).unwrap();
    let mut names = Names::default();
    let mut segs = Segments::default();
    let mcu = Mcu::atmega103();
    let mut matches = vec![];

    Avr::decode(&reg, 0, &mcu, &mut names, &mut segs, &mut matches).unwrap();
    assert_eq!(matches.len(), 1);
    assert_eq!(matches[0].jumps.len(), 1);
    assert_eq!(matches[0].jumps[0].1, Value::val(0x1770, mcu.pc_bits + 1).unwrap());
}

#[test]
fn wrap_around() {
    let _ = simple_logger::init();
    let fd = File::open("tests/data/avr-overflow.bin").unwrap();
    let reg = Region::from_file("", 22, fd, None).unwrap();
    let mut names = Names::default();
    let mut segs = Segments::default();
    let mcu = Mcu::atmega16();
    let mut matches = vec![];

    Avr::decode(&reg, 0, &mcu, &mut names, &mut segs, &mut matches).unwrap();
    assert_eq!(matches.len(), 1);
    assert_eq!(matches[0].jumps.len(), 1);
    assert_eq!(matches[0].jumps[0].1, Value::val(0x1ffe, mcu.pc_bits + 1).unwrap());

    matches.clear();

    Avr::decode(&reg, 0x1ffe, &mcu, &mut names, &mut segs, &mut matches).unwrap();
    assert_eq!(matches.len(), 1);
    assert_eq!(matches[0].jumps.len(), 1);
    assert_eq!(matches[0].jumps[0].1, Value::val(0, mcu.pc_bits + 1).unwrap());
}
